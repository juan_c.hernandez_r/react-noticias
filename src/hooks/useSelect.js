import React, { useState } from "react";
import PropTypes from "prop-types";

const useSelect = (stateInicial, opciones) => {
  // State del custom hook
  const [state, setState] = useState(stateInicial);

  const SelectNoticias = () => (
    <select
      className="browser-default"
      value={stateInicial}
      onChange={(e) => setState(e.target.value)}
    >
      {opciones.map((opcion) => (
        <option key={opcion.cod} value={opcion.value}>
          {opcion.label}
        </option>
      ))}
    </select>
  );
  return [state, SelectNoticias];
};

export default useSelect;
